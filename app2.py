import requests
from flask import Flask

app = Flask(__name__)

@app.route("/")
def home():
    try:
        res = '2 => ' + requests.get("http://app3:5000").text
    except:
        res = 'Ошибка обращения из app2 к app3'
    return res
