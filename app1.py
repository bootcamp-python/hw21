import requests
from flask import Flask

app = Flask(__name__)

@app.route("/")
def home():
    try:
        res = '1 => ' + requests.get("http://app2:5000").text
    except:
        res = 'Ошибка обращения из app1 к app2'
    return res
